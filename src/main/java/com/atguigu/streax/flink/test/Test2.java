package com.atguigu.streax.flink.test;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Test2 {
    public static void main(String[] args) {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        tableEnv.executeSql("create table user_log(" +
                "user_id BIGINT," +
                "item_id BIGINT," +
                "category_id BIGINT," +
                "behavior varchar," +
                "ts varchar)" +
                "with(" +
                "'connector'='kafka'," +
                "'properties.group.id' = 'testGroup_1'," +
                "'properties.enable.auto.commit'='false'," +
                "'topic'='userbehavior'," +
                "'properties.bootstrap.servers'='hadoop101:9092,hadoop102:9092,hadoop103:9092'," +
                "'scan.startup.mode'='earliest-offset'," +
                "'format'='json')");
        tableEnv.executeSql("CREATE TABLE user_log_mysql (" +
                "    user_id BIGINT," +
                "    item_id BIGINT," +
                "    category_id BIGINT," +
                "    behavior varchar," +
                "    ts varchar" +
                " ) WITH (" +
                "'connector.type' = 'jdbc', " +
                "'connector.url' = 'jdbc:mysql://streamx:3306/test'," +
                "'connector.table' = 'user_log', " +
                "'connector.username' = 'root', " +
                "'connector.password' = '123456', " +
                "'connector.write.flush.max-rows' = '1' " +
                " )");

        tableEnv.executeSql("INSERT INTO user_log_mysql select *from  user_log");

    }
}